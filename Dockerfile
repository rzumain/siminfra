FROM python:3.9-slim

ENV HOSTNAME=postgres
ENV PORT=5432
ENV DATABASE=postgres
ENV USERNAME=postgres
ENV PASSWORD=postgres

WORKDIR /opt

RUN pip install --no-cache-dir mlflow boto3 psycopg2-binary

COPY . .
CMD mlflow server \
    --backend-store-uri postgresql://$USERNAME:$PASSWORD@$HOSTNAME:$PORT/$DATABASE \
    --default-artifact-root s3://mlflow \
    --host 0.0.0.0
