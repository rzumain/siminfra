# siminfra

## Getting started

docker-compose up -d

## MLflow Usage

```
EXP_NAME = "model-example"
MODEL_NAME = "mlflow-sample-model"
ART_PATH = "regressor"
```

Set experiment tracking by name:
```
mlflow.set_experiment(experiment_name=EXP_NAME)
with mlflow.start_run(run_name="run-1"):
    reg = linear_model.LinearRegression()
    reg.fit([[0, 0], [1, 1], [2, 2]], [0, 1, 2])

    mlflow.sklearn.log_model(reg, ART_PATH)
```

Get your experiment by name:
```
exp = mlflow.get_experiment_by_name(EXP_NAME)
last_run = mlflow.search_runs(exp.experiment_id, output_format="list")[-1]
print(last_run.info.run_id)
```

Register model by a artifact path:
```
mlflow.register_model(f"runs:/{last_run.info.run_id}/{ART_PATH}", MODEL_NAME)
```
